﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Controlled.Controllers
{
    public class TestController : Controller
    {
        // GET: /<controller>/
        public string Index()
        {
            return "Hello World";
        }

        public List<string> Listed()
        {
            List<string> thelist = new List<string>();
            
            for (int i = 0; i < 20; i++)
            {
                thelist.Add((i + 1).ToString());
            }

            return thelist;
        }

        public int Value()
        {
            Random rnd = new Random();

            return rnd.Next(1, 1000000);
        }
        
        public string TheBest()
        {
            List<string> thelist = new List<string>();
            thelist.Add("a cat");
            thelist.Add("a dog");
            thelist.Add("a fish");
            thelist.Add("a kangaroo");
            thelist.Add("an apple");
            thelist.Add("a rock");

            Random rnd = new Random();

            return "The best animal to have as a pet is " + thelist[rnd.Next(0, 6)];
        }

        public string HorrorStory()
        {
            List<string> goosebumps = new List<string>();
            goosebumps.Add("I begin tucking him into bed and he tells me, “Daddy check for monsters under my bed.” I look underneath for his amusement and see him, another him, under the bed, staring back at me quivering and whispering, “Daddy there’s somebody on my bed.”");
            goosebumps.Add("The doctors told the amputee he might experience a phantom limb from time to time. Nobody prepared him for the moments though, when he felt cold fingers brush across his phantom hand.");
            goosebumps.Add("I can’t move, breathe, speak or hear and it’s so dark all the time. If I knew it would be this lonely, I would have been cremated instead.");
            goosebumps.Add("Don’t be scared of the monsters, just look for them. Look to your left, to your right, under your bed, behind your dresser, in your closet but never look up, she hates being seen.");
            goosebumps.Add("I woke up to hear knocking on glass. At first, I though it was the window until I heard it come from the mirror again.");
            goosebumps.Add("They celebrated the first successful cryogenic freezing. He had no way of letting them know he was still conscious.");
            goosebumps.Add("She wondered why she was casting two shadows. Afterall, there was only a single lightbulb.");
            goosebumps.Add("It sat on my shelf, with thoughtless porcelain eyes and the prettiest pink doll dress I could find. Why did she have to be born still?");
            goosebumps.Add("The grinning face stared at me from the darkness beyond my bedroom window. I live on the 14th floor.");
            goosebumps.Add("There was a picture in my phone of me sleeping. I live alone.");

            Random rnd = new Random();

            return goosebumps[rnd.Next(0, 10)];
        }
    }
}
